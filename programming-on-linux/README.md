# Pemrograman di Linux

## C & C++
### Instalasi
```sh
apt-get install build-essential
```

## Python
### Instalasi
```sh
apt-get install python3
```

## NodeJS
### Instalasi
```sh
apt-get install nodejs npm
```

## Java
### Instalasi
```sh
apt install default-jdk
```

## Ruby
### Instalasi
```sh
apt-get install ruby-full
```

## Perl
Terinstall secara default

## Go
### Instalasi
```sh
apt-get install wget
```
```sh
wget https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
```
```sh
rm -rf /usr/local/go && tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
```
Tambahkan ke $HOME/.profile
```sh
export PATH=$PATH:/usr/local/go/bin
```
Lalu jalankan
```sh
. $HOME/.profile
```


## PHP
### Instalasi
```sh
apt-get install php7.4
```
