# IF214009 Praktikum Sistem Operasi

## Learning Outcome
- Mampu mendemonstrasikan cara kerja sistem operasi melalui perangkat simulasi sistem operasi
- Mampu mendemonstrasikan penggunaan sistem operasi melalui command line interface

## Bahasan
- Structure
- Program
- Process
- System call
- Thread
- Scheduling
- Synchronization
- Dead lock
- Virtual memory management
- File system
- Input output
- Security
- OS Optimization for cloud and backend development
- CLI
- Windows
- Linux

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | Instalasi CPU-OS Simulator, MobaXterm | [CPU-OS Simulator](https://teach-sim.com/), [MobaXTerm](https://mobaxterm.mobatek.net/download.html), [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) 
2 | Perintah Linux dasar, mode file dan folder | [Materi](./linux)
3 | Resource monitoring | [Materi](./resource-monitoring)
4 | |
5 | |
6 | |
7 | |
8 | UTS |
9 | |
10 | |
11 | |
12 | Containerization basic | [Materi](./containerization-basic)
13 | |
14 | |
15 | |
16 | UAS |

## Tools
- [CPU-OS Simulator](https://teach-sim.com/)
- SSH Client
    - [MobaXTerm](https://mobaxterm.mobatek.net/download.html)
- Linux
- Windows

## Referensi

### Materi
- [Guru99 - Operating System](https://www.guru99.com/os-tutorial.html)
- [Geeksforgeeks - Operating System](https://www.geeksforgeeks.org/operating-systems/)
- [Tutorialspoint - Operating System](https://www.tutorialspoint.com/operating_system/os_processes.htm)
- [Stodytonight - Operating System](https://www.studytonight.com/operating-system/)
- [Github top 100 stars](https://github.com/EvanLi/Github-Ranking/blob/master/Top100/Top-100-stars.md)

### Instalasi Tools
- [OS Simulator menggunakan Wine di Linux](https://teachsim.files.wordpress.com/2021/06/linux-and-mac-installation-information.pdf)

### Kurikulum
- [Linux syllabus : Latest Updated Syllabus for syllabus](https://www.shiksha.com/it-software/linux-syllabus-chp)
- [Complete Linux Training Course to Get Your Dream IT Job](https://www.alimco.in/WriteReadData/Recruitment_Document/qrqw1/1_syllabus.pdf)
- [Linux Course Syllabus](https://www.softlogicsys.in/linux-course-content-and-syllabus/)
- [SAP UUI](https://daa.uui.ac.id/download/sap-2-sistem-operasi-komputer-(ganjil-2013-2014)-229.pdf)
- [UTI](https://spada.teknokrat.ac.id/course/view.php?id=739&section=1)
