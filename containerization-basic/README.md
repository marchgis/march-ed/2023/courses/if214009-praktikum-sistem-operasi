# Containerization Basic

## Docker Basic

### Melihat list container yang sedang berjalan
```sh
docker ps
```

### Memonitor penggunaan resource
```sh
docker stats
```

## Linux di Docker

### Membuat container Linux baru yang terus berjalan di background
Contoh di bawah ini
- Nama containernya 13106061
- Port 80 di dalam container di hubungkan dengan port 6061 di host, agar dapat diakses oleh host di port 6061
- Nama imagenya debian, tag nya bullseye-slim
- Menggunakan network yang sudah ada yaitu, niat-yang-suci

```sh
docker run -t -d -v ${PWD}:/home -p 6061:80 --name 13106061 --network niat-yang-suci debian:bullseye-slim
```

### Masuk ke dalam Linux pada container menggunakan Bash
```sh
docker exec -it nama_containernya bash
```

### Update list package Linux di dalam container Debian
Setelah berhasil masuk ke dalam Linux pada container, mulai update list package
```sh
apt-get update
```

### Instalasi program di Linux di dalam container Debian
#### Contoh instalasi git
```sh
apt-get install git
```

#### Contoh instalasi curl dan wget untuk melakukan request HTTP seperti mendownload file atau menarik data
```sh
apt-get install curl wget
```

#### Contoh instalasi text editor Neovim
```sh
apt-get install neovim
```
Neovim dapat dibuka dengan perintah **nvim**

#### Contoh instalasi zsh dan oh-my-zsh
zsh dan oh-my-zsh untuk mempermudah penggunaan command line, serta membuat tampilan command line lebih menarik
1. Install zsh
```sh
apt-get install zsh
```
2. Install oh-my-zsh
```sh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### Mengakses perintah yang sebelumnya dimasukkan
Tekan **tombol panah atas** pada keyboard untuk melihat perintah-perintah yang dimasukan sebelumnya

## Web Server pada Linux di Docker Container
### Nginx

#### Contoh instalasi web server Nginx
```sh
apt-get install nginx
```

#### Menjalankan service Nginx
```sh
service nginx start
```

#### Mengubah halaman index.html menggunakan text editor Neovim
Buat kreasi halaman html, lalu save
```sh
nvim /var/www/html/index.html
```
Buka via publik melalui IP:PORT
