# Business Pitch - Membangun Docker Image Berisi Web Service dan Menjalankannya Sebagai Container Menggunakan Docker Compose

## Capaian
1. Dengan membuat Dockerfile terlebih dahulu, bangun Docker Image berisi:
    - Git
    - Text editor berbasis command line, contoh: Neovim / Vim / Nano
    - Tools pengecekan network, contoh: Netcat
    - Web page / web service sederhana
        - Tema:
            - Solusi dari masalah yang mengancam keselamatan dunia akhiratnya umat manusia, contoh:
                - Keimanan dan ketaqwaan
                - Bencana
                - Penyakit, wabah, pandemi, dsb.
                - Kesehatan fisik, mental, spiritual
                - Kecelakaan
                - Kriminalitas
                - Komunikasi
            - Solusi yang memaksimalkan profit untuk industri
                - Solusi untuk UMKM
                - Optimasi penggunaan sumber daya di industri
                - Rekomendasi kebijakan
            - Solusi dari masalah-masalahmu sehari-hari
        - Tips:
            - Coding web page / web service dapat dilakukan terlebih dahulu, setelah oke, masukan sefolder dengan Dockerfile nya
2. Buat file docker-compose.yml untuk menjalankan Docker Image tersebut menggunakan Docker Compose, dengan PORT yang diekspose adalah PORT yang sebelumnya dibuat + 4000. Contoh, sebelumnya publish di PORT 21103, sekarang publish di PORT 25103.
3. Jalankan menggunakan perintah docker compose hingga container tersebut berjalan dan PORT dapat dibuka dari publik
4. Demokan web page / web service yang telah dapat diakses public tersebut dalam bentuk video Youtube
5. Save project ini di Github / Gitlab, dilengkapi README.md berisi:
    - Deskripsi project
    - Penjelasan bagaimana sistem operasi digunakan dalam proses containerization
    - Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi
    - Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi
    - Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)
6. Daftar dan publikasikan Docker Image yang telah dibuat ke Docker Hub

## Referensi
- Cara membuat Docker Image:
    - [How to Build Docker Image : Comprehensive Beginners Guide - DevOpsCube](https://devopscube.com/build-docker-image/)
    - [Docker Build: A Beginner’s Guide to Building Docker Images - Stackify](https://stackify.com/docker-build-a-beginners-guide-to-building-docker-images/)
    - [How to Build Applications with Docker Compose](https://www.sumologic.com/blog/how-to-build-applications-docker-compose/)
- Cara menjalankan container via Docker Compose
    - [Try Docker Compose](https://docs.docker.com/compose/gettingstarted/)
