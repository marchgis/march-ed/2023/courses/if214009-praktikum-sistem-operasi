# Apa itu Shell Script ?
- Shell Script merupakan file yang berisikan serangkaian perintah untuk dieksekusi oleh program berbasis command line
- Shell Script umum digunakan untuk mengotomasi pekerjaan kompleks via command line seperti:
    - Mempublikasikan aplikasi dari tahap development ke tahap production
    - Mengotomasi pembuatan struktur folder dan file untuk pengembangan aplikasi
    - Mentransformasi data
    - Migrasi data dari satu server ke server lainnya
    - Menulis berbagai perintah untuk penjadwalan
    - Mengotomasi pembersihan data pada server atau komputer pribadi
    - Membuat installler aplikasi
    - Melakukan pengecekan dan monitoring sistem
    - dll  

# Contoh Shell Script
```sh
#!/bin/bash

echo "halo aku agen AI yang akan mengotomasi banyak hal"

echo "sekarang kita ada di: $(pwd)"

# $N adalah untuk mendapatkan argumen ke N
# $1 : mendapatkan argumen ke 1
# $2 : mendapatkan argumen ke 2 dsb.
# Contoh: ./otomasi.sh mobile-legends-game selena
echo "sekarang aku akan membuat folder $1 dengan karakter $2"

echo "Aku buat dulu folder $1 nya"
mkdir -p $1
echo ">> Folder $1 telah dibuat"
ls

echo "Aku tambahkan file $2 di dalam folder $1"
echo "Level: 3\nAttack: 250\nDefense: 100" > $1/$2
echo ">> File $1/$2 telah dibuat"
ls $1
```

# Referensi
- [Steps to write and execute a script - Javatpoint](https://www.javatpoint.com/steps-to-write-and-execute-a-shell-script)
- [Shell Scripting - Tutorialspoint](https://www.tutorialspoint.com/unix/shell_scripting.htm)
- [Introduction to Shell Scripting - Guru 99](https://www.guru99.com/introduction-to-shell-scripting.html)
- [How to Write Bash Scripts in Linux - Freecodecamp](https://www.freecodecamp.org/news/shell-scripting-crash-course-how-to-write-bash-scripts-in-linux/)
